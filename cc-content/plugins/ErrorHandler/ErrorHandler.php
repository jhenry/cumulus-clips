<?php

class ErrorHandler extends PluginAbstract
{
	/**
	* @var string Name of plugin
	*/
	public $name = 'ErrorHandler';

	/**
	* @var string Description of plugin
	*/
	public $description = 'Handles ze Errors.';

	/**
	* @var string Name of plugin author
	*/
	public $author = 'Justin Henry';

	/**
	* @var string URL to plugin's website
	*/
	public $url = 'https://uvm.edu/~jhenry/';

	/**
	* @var string Current version of plugin
	*/
	public $version = '0.0.1';
	
	/**
	* The plugin's gateway into codebase. Place plugin hook attachments here.
	*/	
	public function load(){
		Plugin::attachEvent ( 'account.end' , array( __CLASS__ , 'load_diagnostics' ) );	
		// Nothing to load.
	}


        /**
        * Pretty print vars for more convenient debugging.
        *
        * @var object/array to print
        *
        */
        public function load_diagnostics($var=false) {
		
                echo "<pre>";
		$config = Registry::get('config');
		echo "Configs: ";
		var_dump($config);
                echo " ============================= ";
		echo "UPLOAD_PATH: " . UPLOAD_PATH;
                echo " ============================= ";
		
                var_dump($_SESSION);
                echo " ============================= ";
                var_dump($var);
                echo "</pre>";
        }


        /**
        * Handle and display an error in the proper context.
        *
        * @var string Error message to display.
        *
        */
        public function do_exception($errorMessage)
        {
                //TODO: Hook into filter for system error view
                echo "Error:" . $errorMessage;
                exit;
        }
}

